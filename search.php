<?php
/*
**************************************
*
* Project: Movie Recommendations.
* Client: Switch Media
* Version: v1.0.0
* Created by: Siva Kumar (siva.aus@gmail.com)
* Created on: 31/10/2018 10:00:00
* Modified on: 31/10/2018 12:00:00 
*
*************************************
*/

//$json_file = '.\recommended.json';
$json_file = "https://pastebin.com/raw/cVyp3McN";
$show_time_ahead = "+30 minutes";

$show_type = trim($_POST['search_type']);
$show_time = trim($_POST['search_time']);

//validate for empty value
if( $show_type != '' && $show_time != '') {
	//read the json file
	$movie_data = readRcommendationData($json_file);
	
	$search_data = array();
	$search_data['show_type'] = $show_type;
	$search_data['show_time'] = $show_time;
	$search_data['show_time_ahead'] = $show_time_ahead;
	
	//search show in the json data 
	$search_result = findInRecommendationArray($search_data, $movie_data);
	
	//display result
	displayResult($search_result);
	
} else {
	//error message => Please enter valid data.;
	displayErrorMsg($err=3);
}


//read the json and convert into array
function readRcommendationData($file)
{

	//read the json data
	$content = file_get_contents($file, FALSE);
	
	if(!$content){
		displayErrorMsg($err=1);
		exit;
	}
		
	//decode json string to array
	$json_array = json_decode($content, true);
	
	//validate json array is not empty
	if ( count($json_array) != 0 ) {
		$return = $json_array;
	}else{
		displayErrorMsg($err=2);
		exit;
	}

	return $return;
}

//search in movie array data
function findInRecommendationArray($search_data,$json_array)
{
	$search_type = $search_data['show_type'];
	$search_time = $search_data['show_time'];
	$search_time_ahead = $search_data['show_time_ahead'];

	//find the search ahead time;	
	$search_time_new = date("H:i",strtotime($search_time_ahead,strtotime($search_time)));
	
	$search_result = array();
	
	foreach ($json_array as $shows ){
		
		//search for type - change the all values to lowercase for search
		$response_type = in_array(strtolower($search_type), array_map('strtolower', $shows['genres']));
		if ($response_type){

			//search for time
			foreach ($shows['showings'] as $value_time){
				
				//convert the time 
				$show_time = convertTime($value_time);
				
				if ( $search_time_new < $show_time['24hours']  ){
					$search_result[] = $shows['name'] . ', showing at ' . $show_time['12hours'];
					break;
				}
			} 
		}
	}

	//result validation
	if( count($search_result) == 0 ) $search_result[] = 'No Movie Recommendations.';
	
	return $search_result;	
	
}

//convert the time format
function convertTime($time)
{
	//get the time
	$common_time = explode("+",$time);
	$gmt_time = $common_time[0];
	$gmt_zone = $common_time[1];
	
	$new_time_24 = date("H:i", strtotime($gmt_time));  //24 hours format
	$new_time_12 = date("h:i a", strtotime($gmt_time)); // 12 hours format

	$new_time = array();
	$new_time['12hours'] = $new_time_12;
	$new_time['24hours'] = $new_time_24;
	
	return $new_time;
}

//convert the time
function convertGmtTime($time)
{
	//get the GMT time
	$common_time = explode("+",$time);
	$gmt_time = $common_time[0];
	$gmt_zone = $common_time[1];
	
	//get gmt time zone diffrent in seccond
	$gmt_zone_diff = explode(":", $gmt_zone);
	$gmt_zone_diff_sec = "+".(($gmt_zone_diff[0]*60) + ($gmt_zone_diff[1]))." minutes";
	
	
	$string_time = strtotime($gmt_zone_diff_sec,strtotime($gmt_time));
	$new_time_24 = date("H:i", $string_time);
	$new_time_12 = date("h:i a", $string_time);
	
	$new_time = array();
	$new_time['12hours'] = $new_time_12;
	$new_time['24hours'] = $new_time_24;
	
	return $new_time;
}

// display the result
function displayResult($data){
	
	echo "<BR> <b>Movie Recommendations</b><br>";
	foreach($data as $show){
		echo "<BR>  ". $show;
	}
}

//display Error Msg
function displayErrorMsg($err){
	switch ($err){
		case "1":
			echo "<br> File Not Found.";
			break;
		case "2":
			echo "<br> No Record Found.";
			break;
		case "3":
			echo "<br> Please enter valid data.";
			break;
	}
}

?>